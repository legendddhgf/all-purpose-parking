package com.icherdak.cmps121.parking;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class AddLocActivity extends ActionBarActivity implements GoogleMap.OnMapClickListener{

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private LatLng initLoc; // From intent extras
    Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        initLoc = new LatLng(bundle.getDouble("lat"), bundle.getDouble("lng"));
        setContentView(R.layout.activity_add_loc);
        MapView mapView = (MapView) findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch(Exception e) {
            e.printStackTrace();
        }
        mMap = mapView.getMap();
        mMap.setMyLocationEnabled(true);
        mMap.setOnMapClickListener(this);
        onMapClick(initLoc);
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                .target(initLoc).zoom(16).build()));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onMapClick(LatLng point) {
        if(marker != null) marker.remove();
        marker = mMap.addMarker(new MarkerOptions().position(point).title(point.toString()));
        ((TextView) findViewById(R.id.text_addloc_loc)).setText(point.toString());
    }

    public void onClicker(View view) {
        Context context = getApplicationContext();
        switch(view.getId()) {
            case R.id.button_addloc:
                CoorList tDatabase = new CoorList();
                String name = f.generateValidPositionName(context,
                        ((EditText) findViewById(R.id.addloc_name_edit)).getText().toString());
                LatLng position = marker.getPosition();
                tDatabase.change(name, position.latitude, position.longitude);
                tDatabase.updateToDatabase(0, name);
                Toast.makeText(context, "Added '" + name + "' to parking database.", Toast.LENGTH_SHORT).show();
                super.finish();
                break;
            default:
                break;
        }
    }
}
