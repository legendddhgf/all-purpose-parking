package com.icherdak.cmps121.parking;

/**
 * Created by legendddhgf on 9/10/2015.
 * Meant to always contain the same head and tail no matter where constructed
 * This CoorList is intended to contain EVERY single location in every List stored as CoorData data
 */

public class CoorList {
    private static CoorData head;
    private static CoorData tail;
    private static int size;
    private final static List Databases[] = new List[3]; // an array containing all databases to be synchronized with this CoorList
    // select = 0: dDatabase, select = 1: sDatabase, select = 2: aDatabase

    // make it so the above comment is true
    CoorList(List Database1, List Database2, List Database3) {
        head = null;
        tail = null;
        size = 0;
        Databases[0] = Database1;
        Databases[1] = Database2;
        Databases[2] = Database3;
        updateFromDatabase(0);
        updateFromDatabase(1);
        updateFromDatabase(2);
    }

    // To be used from the second construction onward
    CoorList() {
    }

    /*
    // Returns true if Database was added and false otherwise
    boolean addDatabase(List Database) {
        int i;
        for (i = 0; i < Databases.length; i++) {
            if (Databases[i] == Database) return false;
            if (Databases[i] == null) break;
        }
        Databases[i] = Database;
        return true;
    }*/ // Shouldn't work now that I have the List[] as a static final variable

    // Updates data from a List Database
    // Be Careful using this function!
    void updateFromDatabase(int select) {
        for (int i = 0; i < Databases[select].length(); i++)
            change(Databases[select].getDesc(i), Databases[select].get(i)[0], Databases[select].get(i)[1]);
    }

    // updates from all databases
    void updateFromDatabase() {
        updateFromDatabase(0);
        updateFromDatabase(1);
        updateFromDatabase(2);
    }

    // Updates an index to a List Database
    // Be careful using this function!
    void updateToDatabase(int select, int index) {
        CoorData temp = getData(index);
        Databases[select].change(temp.desc, f.getData(temp.lat, temp.lon));
    }

    // Updates an index to a List Database
    // Be careful using this function!
    void updateToDatabase(int select, String desc) {
        CoorData temp = getData(desc);
        Databases[select].change(temp.desc, f.getData(temp.lat, temp.lon));
    }

    // Self explanatory at this point
    void removefromDatabase(int select, String desc) {
        Databases[select].remove(desc);
    }

    void clearDatabase(int select) {
        Databases[select].clear();
    }

    int length() {return size;}

    // Removes and recreates a CoorData to have the given desc, lat, and lon
    // Creates it if it does not yet exist
    // don't forget index starts at 0
    void change(String desc, double lat, double lon) {
        remove(desc); // size-- is embedded in remove assuming that it is successful
        CoorData n = new CoorData(desc, lat, lon);
        if (tail == null) {
            tail = head = n;
        } else {
            tail.next = n;
            n.prev = tail;
            tail = tail.next;
        }
        //System.out.println(desc + " has been added to the tDataBase with coordinates: " + lat + ", " + lon);
        size++; // this cancels with size-- if an item was changed rather than added
    }

    // returns 'true' if item was found and removed from list and 'false' otherwise
    @SuppressWarnings("StatementWithEmptyBody")
    boolean remove(String desc) {
        CoorData temp;
        for (temp = head; temp != null && !temp.desc.equals(desc); temp = temp.next) ;
        if (temp != null) {
            if (size == 1) clear();
            else {
                if (temp == head) head = head.next;
                else temp.prev.next = temp.next;
                if (temp == tail) tail = tail.prev;
                else temp.next.prev = temp.prev;
                size--;
            }
            return true;
        }
        return false;
    }

    // returns true if database exists, and false otherwise
    boolean exists(String desc) {
        for (CoorData temp = head; temp != null; temp = temp.next)
            if (temp.desc.equals(desc)) return true;
        return false;
    }

    void clear() {
        head = null;
        tail = null;
        size = 0;
    }

    // returns a Coordata object with reference to a specific index
    //
    CoorData getData(String desc) {
        CoorData temp;
        for (temp = head; temp != null; temp = temp.next) if (temp.desc.equals(desc)) break;
        return temp;
    }

    // returns a CoorData object with reference to s specific index
    // Will return null if index >= size
    CoorData getData(int index) {
        if (index < 0) index = 0; // index can't be less than 0
        CoorData temp;
        int i = 0;
        for (temp = head; temp != null; temp = temp.next) if (i++ == index) break;
        return temp;
    }

    double getLat(String desc) {
        if (getData(desc) == null)
            throw new RuntimeException("Tried to getLat() from a non-existent data");
        return getData(desc).lat;
    }

    double getLon(String desc) {
        if (getData(desc) == null)
            throw new RuntimeException("Tried to getLon() from a non-existent data");
        return getData(desc).lon;
    }

    String getDesc(int index) {
        if (getData(index) == null)
            throw new RuntimeException("Tried to getDesc() from a non-existent data");
        return getData(index).desc;
    }

    public String toString() {
        String temp = "";
        for (CoorData tempn = head; tempn != null; tempn = tempn.next)
            temp += tempn.desc + " = " + tempn.lat + " " + tempn.lon + "\n";
        return temp;
    }

    public String dataBaseToString(int select) {
        return Databases[select].toString();
    }
}
