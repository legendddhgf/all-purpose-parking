package com.icherdak.cmps121.parking;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Loc_List extends AppCompatActivity {

    private Location lastLoc;

    static CoorList tDatabase;

    int maxLoc;

    int maxDist;

    private class ListElement {
        ListElement() {
        }

        public String textLabel;
        public String locationLabel;
    }

    private class MyAdapter extends ArrayAdapter<ListElement> {

        int resource;
        Context context;

        public MyAdapter(Context _context, int _resource, ArrayList<ListElement> items) {
            super(_context, _resource, items);
            resource = _resource;
            context = _context;
            this.context = _context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LinearLayout newView;

            final ListElement w = getItem(position);

            // Inflate a new view if necessary.
            if (convertView == null) {
                newView = new LinearLayout(getContext());
                String inflater = Context.LAYOUT_INFLATER_SERVICE;
                LayoutInflater vi = (LayoutInflater) getContext().getSystemService(inflater);
                vi.inflate(resource, newView, true);
            } else {
                newView = (LinearLayout) convertView;
            }

            // Fills in the view.
            TextView tv = (TextView) newView.findViewById(R.id.itemText);
            TextView tv2 = (TextView) newView.findViewById(R.id.itemLocation);
            tv.setText(w.textLabel);
            tv2.setText(w.locationLabel);

            // Set a listener for the whole list item.
            newView.setTag(w.textLabel);
            newView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String gmapLink = "http://maps.google.com/maps?q=loc:" +
                            tDatabase.getLat(w.textLabel) + "," + tDatabase.getLon(w.textLabel) + " (" + v.getTag().toString() + ")";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(gmapLink));
                    context.startActivity(intent);
                    /*if(lastLoc != null) {
                        String gmapLink = "http://maps.google.com/maps?q=loc:" +
                                lastLoc.getLatitude() + "," + lastLoc.getLongitude() + " (" + v.getTag().toString() + ")";
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(gmapLink));
                        context.startActivity(intent);
                    } else {
                        // TODO: Add more elegant error handling for lastLoc == null
                        String s = v.getTag().toString();
                        s += ": " + (lastLoc == null || tDatabase.getData(s) == null ? " an undetermined distance away" : f.mgetDist(f.getData(tDatabase.getData(v.getTag().toString())), f.getData(lastLoc)) + " miles away");
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, s, duration);
                        toast.show();
                    }*/
                }
            });

            return newView;
        }
    }

    private MyAdapter aa;

    private ArrayList<ListElement> aList;

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            boolean isBetter = ((lastLoc == null) ||
                    location.getAccuracy() < lastLoc.getAccuracy() + (location.getTime() - lastLoc.getTime()));
            if (isBetter) {
                // Replace location
                lastLoc = location;
                aList.clear();
                List temp = f.closestLocations(tDatabase, lastLoc, maxDist, maxLoc);
                for (int i = 0; i < temp.length(); i++) {
                    ListElement ael = new ListElement();
                    ael.textLabel = temp.getDesc(i);
                    ael.locationLabel = lastLoc != null ? f.mgetDist(temp.get(ael.textLabel), f.getData(lastLoc)) + " miles" : " ? miles";
                    aList.add(ael);
                }
                aa.notifyDataSetChanged();
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };


    public SharedPreferences settings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loc__list);
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        maxLoc = Integer.parseInt(settings.getString("pref_max_loc", "20"));
        maxDist = Integer.parseInt(settings.getString("pref_max_dist", "25"));
        aList = new ArrayList<>();
        aa = new MyAdapter(this, R.layout.list_element, aList);
        ListView myListView = (ListView) findViewById(R.id.LoclistView);
        myListView.setAdapter(aa);
        aa.notifyDataSetChanged();
        tDatabase = new CoorList();
        aList.clear();
        List temp = f.closestLocations(tDatabase, lastLoc, maxDist, maxLoc);
        for (int i = 0; i < temp.length(); i++) {
            ListElement ael = new ListElement();
            ael.textLabel = temp.getDesc(i);
            ael.locationLabel = lastLoc != null ? f.mgetDist(temp.get(ael.textLabel), f.getData(lastLoc)) + " miles" : " ? miles";
            aList.add(ael);
        }
        aa.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        maxLoc = Integer.parseInt(settings.getString("pref_max_loc", "20"));
        maxDist = Integer.parseInt(settings.getString("pref_max_dist", "25"));
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_loc__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
