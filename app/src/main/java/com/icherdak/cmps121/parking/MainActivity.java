package com.icherdak.cmps121.parking;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    public static List sDatabase; // static database meant to have all predefined locations

    public static List dDatabase; // dynamic database meant to contain all user defined locations

    public static List aDatabase; // anonymous database meant to contain previous parking locations

    public static CoorList tDatabase; // CoorList static database which should contain all parking locations

    public static Location lastLoc;

    private class ListElement {
        ListElement() {}

        public String textLabel;
        public String locationLabel;
    }

    private class MyAdapter extends ArrayAdapter<ListElement> {

        int resource;
        Context context;

        public MyAdapter(Context _context, int _resource, ArrayList<ListElement> items) {
            super(_context, _resource, items);
            resource = _resource;
            context = _context;
            this.context = _context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LinearLayout newView;

            ListElement w = getItem(position);

            // Inflate a new view if necessary.
            if (convertView == null) {
                newView = new LinearLayout(getContext());
                String inflater = Context.LAYOUT_INFLATER_SERVICE;
                LayoutInflater vi = (LayoutInflater) getContext().getSystemService(inflater);
                vi.inflate(resource,  newView, true);
            } else {
                newView = (LinearLayout) convertView;
            }

            // Fills in the view.
            TextView tv = (TextView) newView.findViewById(R.id.itemText);
            TextView tv2 = (TextView) newView.findViewById(R.id.itemLocation);
            tv.setText(w.textLabel);
            tv2.setText(w.locationLabel);

            // Set a listener for the whole list item.
            newView.setTag(w.textLabel);
            newView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // needs functionality that displays this to map
                    String s = v.getTag().toString();
                    s += ": " + (lastLoc == null  || tDatabase.getData(s) == null ? " an undetermined distance away" : f.mgetDist(f.getData(tDatabase.getData(v.getTag().toString())), f.getData(lastLoc)) + " miles away");
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, s, duration);
                    toast.show();
                }
            });

            return newView;
        }
    }

    private MyAdapter aa;

    private ArrayList<ListElement> aList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lastLoc = null;
        setContentView(R.layout.activity_main);

        /*aList = new ArrayList<>();
        aa = new MyAdapter(this, R.layout.list_element, aList);
        ListView myListView = (ListView) findViewById(R.id.listView);
        myListView.setAdapter(aa);
        aa.notifyDataSetChanged();*/
        dDatabase = new List();
        sDatabase = new List();
        aDatabase = new List();

        PreferenceManager.setDefaultValues(this, R.xml.settings, false);
        locationEnabled = true;

        // addLocation(dDatabase, "Gotta Get That Swag", getData(123.46782, -23.47343), true); // gotta feel the debug
        // The above line was used in debugging to see if file saves were working correctly

        readFile("locations.ini", dDatabase); // read from saved locations
        readFile("parking.ini", aDatabase); // read from saved parking spots

        // dDatabase.remove("Gotta Get That Swag");
        // The above line was used to delete the database which was added a few lines above

        addDefaultLocations(); // add all default locations to static database
        tDatabase = new CoorList(dDatabase, sDatabase, aDatabase); // Remember this order of 0, 1, 2
        //tDatabase.updateToDatabase(0, "Century Theaters Parking"); // check that we can update info back into our lists in MainActivity
        //dDatabase.remove("Century Theaters Parking"); // remove it after it was saved to a text file

        // testing listview
        /*aList.clear();
        ListElement ael = new ListElement();
        ael.textLabel = "Century Theaters Parking";
        ael.locationLabel = ": ? miles";
        aList.add(ael);
        aa.notifyDataSetChanged();
        */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_help).setEnabled(! settings.getBoolean("pref_welcome", false));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.action_help:
                startActivity(new Intent(this, WelcomeActivity.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public SharedPreferences settings;
    @Override
    protected void onResume() {
        super.onResume();
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        settings = PreferenceManager.getDefaultSharedPreferences(this);

        findViewById(R.id.card_welcome).setVisibility(settings.getBoolean("pref_welcome", false) ? View.VISIBLE : View.GONE);
        findViewById(R.id.card_addloc).setVisibility(settings.getBoolean("pref_addloc", false) ? View.VISIBLE : View.GONE);
        findViewById(R.id.card_location).setVisibility(settings.getBoolean("pref_location", false) ? View.VISIBLE : View.GONE);
        findViewById(R.id.button_quickadd).setVisibility(settings.getBoolean("pref_quickadd", false) ? View.VISIBLE : View.GONE);
        ((TextView)findViewById(R.id.text_location)).setText(getString(R.string.text_location_loading));
    }

    @Override
    protected void onPause() {
        super.onPause();
        writeFile("locations.ini", dDatabase); // write back saved locations
        writeFile("parking.ini", aDatabase); // write back saved parking spots
    }

    /**
     * Listenes to the location, and gets the most precise recent location.
     */
    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            // Taken from hw3
            // Is better?
            boolean isBetter = ((lastLoc == null) ||
                    location.getAccuracy() < lastLoc.getAccuracy() + (location.getTime() - lastLoc.getTime()));
            if(isBetter) {
                // Replace location
                lastLoc = location;
                // Update location card
                ((TextView)findViewById(R.id.text_location)).setText(
                        "Latitude: " + lastLoc.getLatitude() +
                        "\nLongitude: " + lastLoc.getLongitude() +
                        "\nAccuracy: ±" + lastLoc.getAccuracy() +
                        "\nLast checked: " + new Time(lastLoc.getTime()));

                // testing listview
                /*aList.clear();
                ListElement ael = new ListElement();
                ael.textLabel = "Century Theaters Parking";
                ael.locationLabel = tDatabase.getData(ael.textLabel) != null ? ": " + mgetDist(f.getData(tDatabase.getData(ael.textLabel)), f.getData(lastLoc)) + " miles" : " ? miles";
                aList.add(ael);
                aa.notifyDataSetChanged();
                */
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
            ((TextView)findViewById(R.id.text_location)).setText(getString(R.string.text_location_loading));
            onLocationEnabled();
        }

        @Override
        public void onProviderDisabled(String provider) {
            ((TextView)findViewById(R.id.text_location)).setText(getString(R.string.text_location_disabled));
            onLocationDisabled();
        }
    };

    // Snackbar code
    public Snackbar snackbar;
    public boolean locationEnabled;
    int[] ids = new int[] {R.id.card_addloc, R.id.card_locator, R.id.card_marker}; // Cards disabled when location off
    public void onLocationDisabled() {
        locationEnabled = false;
        snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Location services is disabled", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Enable", onSnackbarClickListener).show();
        for(int id : ids) {
            CardView card = (CardView) findViewById(id);
            card.setCardBackgroundColor(Color.LTGRAY);
        }
    }
    public void onLocationEnabled() {
        locationEnabled = true;
        if(snackbar != null) snackbar.dismiss();
        for(int id : ids) {
            CardView card = (CardView) findViewById(id);
            card.setCardBackgroundColor(Color.WHITE);
        }
    }
    final View.OnClickListener onSnackbarClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    };

    public void onClicker(View view)  {
        Context context = getApplicationContext();
        String bread = "";
        SharedPreferences.Editor editor = settings.edit();
        String desc;
        Intent intent;
        switch(view.getId()) {
            case R.id.button_welcome_remove:
                editor.putBoolean("pref_welcome", false);
                editor.apply();
                findViewById(R.id.card_welcome).setVisibility(View.GONE);
                break;
            case R.id.button_welcome_dismiss:
                startActivity(new Intent(this, WelcomeActivity.class));
                break;
            case R.id.card_locator:
                if(locationEnabled) {
                    intent = new Intent(this, Loc_List.class);
                    startActivity(intent);
                } else {
                    bread = "Cannot search for a location while Location Services is disabled";
                }
                break;
            case R.id.card_addloc:
                if(locationEnabled && lastLoc != null) {
                    intent = new Intent(this, AddLocActivity.class);
                    intent.putExtra("lat", lastLoc.getLatitude());
                    intent.putExtra("lng", lastLoc.getLongitude());
                    startActivity(intent);
                } else {
                    bread = "Cannot add a location while Location Services is disabled";
                }
                break;
            case R.id.card_marker:
                if(locationEnabled) {
                    intent = new Intent(this, MarkerActivity.class);
                    startActivity(intent);
                } else {
                    bread = "Cannot mark location while Location Services is disabled";
                }
                break;
            case R.id.button_quickadd:
                desc = f.generateValidPositionName(context);
                tDatabase.change(desc, lastLoc.getLatitude(), lastLoc.getLongitude());
                tDatabase.updateToDatabase(0, desc);
                bread = "Added quick location " + desc;
                break;
            default:
                bread = "Unknown card was selected.";
                break;
        }
        if(! bread.equals("")) Toast.makeText(context, bread, Toast.LENGTH_SHORT).show();
    }

    // Function adds default parking locations (originally the main purpose of app)
    public void addDefaultLocations() {
        f.addLocation(sDatabase, "Century 20 Theaters Parking", f.getData(37.703064, -122.470268), true);
        f.addLocation(sDatabase, "110 Franklin St. Parking", f.getData(37.775519, -122.420735), true);
        f.addLocation(sDatabase, "1266 Market St. Parking", f.getData(37.778465, -122.416265), true);
        f.addLocation(sDatabase, "199 Turk St. Parking", f.getData(37.782795, -122.412089), true);
        f.addLocation(sDatabase, "530 Turk St. Parking", f.getData(37.782519, -122.418135), true);
        f.addLocation(sDatabase, "200 Hyde St. Parking", f.getData(37.782710, -122.415536), true);
        f.addLocation(sDatabase, "768 Sansome St. Parking", f.getData(37.797378, -122.401671), true);
        f.addLocation(sDatabase, "First Market Tower Parking", f.getData(37.790506, -122.399164), true);
        f.addLocation(sDatabase, "550 North Point St. Parking", f.getData(37.806832, -122.416263), true);
        f.addLocation(sDatabase, "400 Taylor St. Parking", f.getData(37.786261, -122.411050), true);
        f.addLocation(sDatabase, "1132 Imperial Ave Parking", f.getData(32.706429, -117.154547), true);
        f.addLocation(sDatabase, "San Diego Airport Parking", f.getData(32.750975, -117.201876), true);
        f.addLocation(sDatabase, "UCSD School of Medicine Parking", f.getData(32.873859, -117.236759), true);
        f.addLocation(sDatabase, "Cedar St. Garage Parking", f.getData(36.974982, -122.027473), true);
        f.addLocation(sDatabase, "124 Locust St. Parking", f.getData(36.976045, -122.027504), true);
        f.addLocation(sDatabase, "Soquel/Front Garage Parking", f.getData(36.974153, -122.025079), true);
        f.addLocation(sDatabase, "2700 Brommer St. Parking", f.getData(36.968877, -121.976588), true);
        f.addLocation(sDatabase, "422 Capitola Ave Parking", f.getData(36.975328, -121.953574), true);
        f.addLocation(sDatabase, "UCSC East Remote Parking", f.getData(36.991360, -122.054682), true);
        /*f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);
        f.addLocation(sDatabase, "", f.getData(0, 0), true);*/
    }

    // returns an array of latitude, longitude of last location from static variable
    // somehow feel like this is almost too lazy
    public double[] getData() {
        return f.getData(lastLoc);
    }

    // This function is meant to be called when the program starts up to read all previous database
    // entries from backup. It may be buggy or inefficient in it's currentMarker state so consider revisions
    public void readFile(String filename, List Database) {
        Context context = getApplicationContext();
        Scanner in;
        String str; // string for storing each line;
        String strt; // string to hold description
        String strc; // string to hold one double at a time
        double data[] = new double[2];
        try {
            in = new Scanner(new File(context.getFilesDir(), filename));
        } catch (FileNotFoundException e) {
            try {
                PrintWriter out = new PrintWriter(new File(context.getFilesDir(), filename));
                out.println("Warning: Do Not Edit This File!");
                out.close();
            } catch (FileNotFoundException f) {
                // What do you do when built in methods don't even work?
            }
            return;
        }
        while (in.hasNextLine()) {
            strt = "";
            str = in.nextLine();
            // System.out.println("File: " + filename + " has line: " + str);
            // uncomment the above line to see what each line of your text file says
            if (str.contains(" = ")) {
                int i;
                for (i = 0; i < str.length() && str.charAt(i + 1) != '='; i++) strt += str.charAt(i);
                i += 3; // now we will point to the first number
                strc = "";
                while (i < str.length() && str.charAt(i) != ' ' && str.charAt(i) != '\n') {
                    strc += str.charAt(i++);
                }
                data[0] = Double.parseDouble(strc);
                strc = "";
                i++; // adding this fixed the bug. Basically, it wasn't parsing the second part of the data
                // because when it got to the second while statement, it was on a ' ' character.
                while (i < str.length() && str.charAt(i) != ' ' && str.charAt(i) != '\n') {
                    strc += str.charAt(i++);
                }
                data[1] = Double.parseDouble(strc);
                f.addLocation(Database, strt, data, false); // we may want to consider the possibility of checking
                // for valid database storage in the future
            }
        }
        in.close();
    }

    // writes currentMarker status of parking and manual save databases to a file so info can be saved
    public void writeFile(String filename, List Database) {
        Context context = getApplicationContext();
        PrintWriter out;
        try {
            out = new PrintWriter(new File(context.getFilesDir(), filename));
        } catch (FileNotFoundException e) {
            // what do you do when built in methods don't even work
            return;
        }
        out.println("Warning: Do Not Edit This File!");
        out.print(Database);
        out.close();
    }
}
