package com.icherdak.cmps121.parking;

/**
 * Created by legendddhgf on 9/8/2015.
 * And this template is no longer default
 */
public class CoorData {

    final String desc;
    final double lat;
    final double lon;
    CoorData next;
    CoorData prev;

    CoorData(String desc, double lat, double lon) {
        this.desc = desc;
        this.lat = lat;
        this.lon = lon;
        this.next = null;
        this.prev = null;
    }
}
