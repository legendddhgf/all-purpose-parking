package com.icherdak.cmps121.parking;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;

/**
 * Created by legendddhgf on 9/10/2015.
 * Intended to contain functions useful across all classes
 * class name is short for easy reference
 */
public class f {
    // returns an array of two doubles given the two doubles
    // useful for directly inputting two array elements in one line
    public static double[] getData(double a, double b) {
        double n[] = (new double[2]);
        n[0] = a;
        n[1] = b;
        return n;
    }

    // returns an array of latitude, longitude given a location
    // useful for lazy people
    public static double[] getData(Location location) {
        return (location == null) ? null : getData(location.getLatitude(), location.getLongitude());
    }

    // For even lazier purposes
    static double []getData(CoorData data) {
        return f.getData(data.lat, data.lon);
    }

    public static String generateValidPositionName(Context context, String name) {
        CoorList tDatabase = new CoorList();
        if(name.equals("")) {
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
            name = settings.getString("pref_quickname", "Parking");
        }
        if (tDatabase.exists(name)) {
            int dup = 1;
            while (tDatabase.exists(name + dup)) dup++;
            name = name + dup;
        }
        return name;
    }
    public static String generateValidPositionName(Context context) {
        return generateValidPositionName(context, "");
    }

    // returns distance in meters between two coordinates
    // source: http://andrew.hedges.name/experiments/haversine/
    public static double getDist(double data1[], double data2[]) {
        Location loc1 = new Location("");
        loc1.setLatitude(data1[0]);
        loc1.setLongitude(data1[1]);
        Location loc2 = new Location("");
        loc2.setLatitude(data2[0]);
        loc2.setLongitude(data2[1]);
        return loc2.distanceTo(loc1);
    }

    // when conversion is below you
    public static double getDist(CoorData data1, CoorData data2) {
        return getDist(new double[] {data1.lat, data1.lon}, new double[] {data2.lat, data2.lon});
    }

    // returns distance but in miles same as getDist() but also rounds to hundreth's place
    public static double mgetDist(double data1[], double data2[]) {
        return (double) Math.round(0.000621371 * getDist(data1, data2) * 100) / 100;
    }

    // when conversion is below you
    public static double mgetDist(CoorData data1, CoorData data2) {
        return (double) Math.round(0.000621371 * getDist(data1, data2) * 100) / 100;
    }

    // adds description and coordinates as a new parking location
    // Program will overwrite existing locations with currentMarker coordinates if force is true
    // returns true if location was successfully saved into database and false otherwise
    // It should be noted that this function is intended to be used with the dDatabase
    // and hence should only be used in adding user managed databases to the app
    // DO NOT ADD LOCATIONS WITH '=' IN STRING NAMES
    public static boolean addLocation(List Database, String loc, double data[], boolean force) {
        return ((force || !Database.exists(loc)) && Database.change(loc, data));
    }

    // use below prototypes as reference
    // if location is not valid (null) then it will be assumed that a location is in the appropriate proximity
    public static List closestLocations(CoorList Database, Location lastLoc, int proximity, int maxDisp) {
        Boolean vloc = lastLoc != null;
        CoorData temp;
        if (vloc) temp = new CoorData("temp", lastLoc.getLatitude(), lastLoc.getLongitude());
        else temp = null;
        List n = new List();
        if (Database.length() < 1) return n;
        if (!vloc) n.change(Database.getDesc(0), getData(Database.getData(0)));
        else if (mgetDist(Database.getData(0), temp) < proximity) n.change(Database.getDesc(0), getData(Database.getData(0)));
        for (int i = 1; i < Database.length() && n.length() < maxDisp; i++) {
            if (!vloc) n.change(Database.getDesc(i), getData(Database.getData(i)));
            else if (mgetDist(Database.getData(i), temp) <= proximity) n.change(Database.getDesc(i), getData(Database.getData(i)));
        }
        n.sort(getData(lastLoc));

        //System.out.println("The Current Proximity is " + proximity + " and the Current MaxDisp is " + maxDisp);
        //System.out.print("Closest Locations are:\n" + n.toString());
        return n;
    }

    /* // deprecated
    // A version of closestLocations() which provides a better return format but works otherwise identical
    // It should be noted that when array indexes are empty the will be set to null
    // This function now returns a CoorData variable which is the head of a list of CoorData variables (instead of an array)
    // May return null if no locations are found based on criteria
    CoorData fclosestLocations(double loc[], int proximity, int maxDisplayed) {
        String udata[][] = closestLocations(loc, proximity, maxDisplayed);
        CoorData head = udata[0][0].equals(udata[0][1]) ? null : new CoorData(udata[0][0], getData(udata, 0)[0], getData(udata, 0)[1]);
        CoorData temp = head;
        for (int i = 1; i < udata.length; i++) {
            if (udata[i][0].equals(udata[i][1])) break;
            else {
                temp.next = new CoorData(udata[i][0], getData(udata, i)[0], getData(udata, i)[1]); // don't worry about dereferences: the if statement above incorporates a check
                temp.next.prev = temp;
                temp = temp.next;
            }
        }
        return head;
    }

    // returns an array which contains a string identifying a location and an identifier telling you
    // which database the data can be found. Use getData() to extract coordinates at a given index
    // Note that there may be less than maxDisplayed location in which case some indexes in the array
    // will have the following property: A[index][0] = A[index][1] = ""
    // This is dependant on how many locations are saved in databases and the proximity (miles) you have chosen
    // from the location specified by loc
    String[][] closestLocations(double loc[], int proximity, int maxDisplayed) {
        if (maxDisplayed < 1)
            maxDisplayed = 1; // Can't allow less than one location to be displayed
        String A[][] = new String[maxDisplayed][2]; // The second slot will tell you in which database you can find the data
        if (f.getDist(sDatabase.get(0), loc) <= proximity) {
            A[0][0] = sDatabase.getDesc(0);
            A[0][1] = "s";
        } else return A;
        for (int j = 0; j < maxDisplayed; j++) {
            int i = 0;
            if (A[j][0].equals(A[j][1])) {
                A[j][0] = A[j - 1][0];
                A[j][1] = A[j - 1][1];
            }
            while (i < dDatabase.length()) {
                if (f.getDist(getData(A, j), loc) > f.getDist(dDatabase.get(i), loc)
                        && f.getDist(dDatabase.get(i), loc) <= proximity) {
                    A[j][0] = dDatabase.getDesc(i);
                    A[j][1] = "d";
                }
            }
            i = 0;
            while (i < sDatabase.length()) {
                if (f.getDist(getData(A, j), loc) > f.getDist(sDatabase.get(i), loc)
                        && f.getDist(sDatabase.get(i), loc) <= proximity) {
                    A[j][0] = sDatabase.getDesc(i);
                    A[j][1] = "s";
                }
            }
            i = 0;
            while (i < aDatabase.length()) {
                if (f.getDist(getData(A, j), loc) > f.getDist(aDatabase.get(i), loc)
                        && f.getDist(aDatabase.get(i), loc) <= proximity) {
                    A[j][0] = aDatabase.getDesc(i);
                    A[j][1] = "a";
                }
            }
            if (j > 0 && A[j][0].equals(A[j - 1][0]) && A[j][1].equals(A[j - 1][1])) {
                A[j][0] = "";
                A[j][1] = "";
                break;
            }
        }
        return A;
    }

    // intended to be used with the array made in closestLocations()
    private double[] getData(String sdata[][], int index) throws RuntimeException {
        if (index >= sdata.length)
            throw new RuntimeException("Used wrong data size in private getData()");
        switch (sdata[index][1]) { // breaks unneeded since only one line of each situation will occur anyway
            case "s":
                return sDatabase.get(sdata[index][0]);
            case "d":
                return dDatabase.get(sdata[index][0]);
            case "a":
                return aDatabase.get(sdata[index][0]);
            default:
                throw new RuntimeException("Used wrong List identifier in private getData(): expected \"s\", \"d\", or \"a\"");
        }
    }*/

    public static boolean descExists(String A[], String k) {
        for (int i = 0; i < A.length; i++) if (A[i] != null && A[i].equals(k)) return true;
        return false;
    }
}