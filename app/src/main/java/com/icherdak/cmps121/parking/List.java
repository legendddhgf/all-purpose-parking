package com.icherdak.cmps121.parking;

/**
 * Created by legendddhgf on 9/1/2015.
 * it should be noted that I am trying to be extra careful with pointers and some code may reflect this
 */
public class List {
    private Node head;
    private Node tail;
    private int size;

    private class Node {
        Node prev;
        Node next;
        double item[]; // 0: latitude, 1; longitude
        String desc; // a string describing the location
        boolean valid; // whether or not an item has valid coordinates

        Node(String desc, double data[]) {
            prev = next = null;
            this.desc = "" + desc; // this is to assure that this.desc is a different pointer
            item = new double[2];
            valid = true;
            if (data.length == item.length) {
                for (int i = 0; i < data.length; i++) item[i] = data[i];
            } else valid = false;
        }

        Node(String desc, double lat, double lon) {
            prev = next = null;
            this.desc = desc;
            item = f.getData(lat, lon);
            valid = true;
        }
    }

    List() {
        head = null;
        tail = null;
        size = 0;
    }

    int length() {
        return size;
    }

    // returns true if change was successful and false otherwise
    /* change is defined as 'false' in the scenario that the resulting Node was either
     * unchanged or left invalid. All other scenarios result in a 'true' output
     */
    // don't forget index starts at 0
    boolean change(String desc, double data[]) {
        Node temp = head;
        while (temp != null && !temp.desc.equals(desc)) {
            temp = temp.next;
        }
        if (temp == null) {
            Node n = new Node(desc, data);
            if (tail == null) {
                tail = head = n;
            } else {
                tail.next = n;
                n.prev = tail;
                tail = tail.next;
            }
        } else if (data.length == temp.item.length) {
            for (int i = 0; i < data.length; i++) temp.item[i] = data[i];
            temp.valid = true;
        } else {
            return false;
        }
        size++;
        return true;
    }

    // returns 'true' if item was found and removed from list and 'false' otherwise
    @SuppressWarnings("StatementWithEmptyBody")
    boolean remove(String desc) {
        Node temp;
        for (temp = head; temp != null && !temp.desc.equals(desc); temp = temp.next) ;
        if (temp != null) {
            if (size == 1) clear();
            else {
                if (temp == head) head = head.next;
                else temp.prev.next = temp.next;
                if (temp == tail) tail = tail.prev;
                else temp.next.prev = temp.prev;
                size--;
            }
            return true;
        }
        return false;
    }

    // I see no reason to consider the possibility of this method failing
    void clear() {
        head = null;
        tail = null;
        size = 0;
    }

    // sorts the list from least to greatest distance to the given location
    void sort(double data[]) {
        if (length() < 2 || data == null) return;
        double min = f.getDist(data, head.item);
        int minPos = 0;
        double max = 0;
        double temp[][] = new double[length()][2];
        String stemp[] = new String[length()];
        for (int i = 0; i < temp.length; i++) {
            if (f.getDist(data, get(i)) < min) {
                min = f.getDist(data, get(i));
                minPos = i;
            }
            if (f.getDist(data, get(i)) > max) max = f.getDist(data, get(i));
        }
        temp[0][0] = get(minPos)[0];
        temp[0][1] = get(minPos)[1];
        stemp[0] = getDesc(minPos);
        for (int i = 1; i < temp.length; i++) {
            double pmin = min;
            min = max;
            for (int j = 0; j < temp.length; j++) {
                if (f.getDist(data, get(j)) <= min && f.getDist(data, get(j)) >= pmin && !f.descExists(stemp, getDesc(j))) {
                    min = f.getDist(data, get(j));
                    minPos = j;
                }
            }
            temp[i][0] = get(minPos)[0];
            temp[i][1] = get(minPos)[1];
            stemp[i] = getDesc(minPos);
        }
        clear();
        for (int i = 0; i < temp.length; i++) change(stemp[i], temp[i]);
        /*System.out.print("The sorted list is:\n" + toString());
        System.out.println("The sorted array is:");
        for (int i = 0; i < temp.length; i++) {
            System.out.println(stemp[i] + " : " + temp[i][0] + " " + temp[i][1]);
        }*/
    }

    // swaps two items in the list
    void swap(Node temp1, Node temp2) {
        String stemp = temp1.desc;
        double dtemp[] = temp1.item;
        temp1.desc = temp2.desc;
        temp1.item = temp2.item;
        temp2.desc = stemp;
        temp2.item = dtemp;
    }

    // returns true if database exists, and false otherwise
    boolean exists(String desc) {
        for (Node temp = head; temp != null; temp = temp.next) if(temp.desc.equals(desc)) return true;
        return false;
    }

    // get method one: allows you to receive coordinates for a location based off it's description.
    // Note that it may return null in the scenario that a location with this description is not yet present
    double[] get(String desc) {
        for (Node temp = head; temp != null; temp = temp.next) if (temp.desc.equals(desc)) return temp.item;
        return null;
    }

    // This will probably be rarely used but I decided to include an additional function which can return
    // coordinates at a given index. Works the same as the above in all other ways
    double[] get(int index) {
        Node temp = head;
        for (int i = 0; temp != null && i < index; i++) temp = temp.next;
        return (temp != null ? temp.item : null);
    }

    String getDesc(int index) {
        Node temp = head;
        for (int i = 0; temp != null && i < index; i++) temp = temp.next;
        return (temp != null ? temp.desc : null);
    }

    public String toString() {
        String temp = "";
        for (Node tempn = head; tempn != null; tempn = tempn.next)
            temp += tempn.desc + " = " + tempn.item[0] + " " + tempn.item[1] + "\n";
        return temp;
    }
}
