package com.icherdak.cmps121.parking;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;

public class MarkerActivity extends AppCompatActivity implements GoogleMap.OnMapClickListener {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    CoorList tDatabase;
    Marker marker;
    Location lastLoc;
    int currentMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        setContentView(R.layout.activity_marker);
        MapView mapView = (MapView) findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch(Exception e) {
            e.printStackTrace();
        }

        tDatabase = new CoorList();
        tDatabase.clear();
        tDatabase.updateFromDatabase(2);

        mMap = mapView.getMap();
        mMap.setMyLocationEnabled(true);
        mMap.setOnMapClickListener(this);
        ((Button) findViewById(R.id.button_parking_newer)).setEnabled(false);
        currentMarker = 0;
        displayMarker(0);
    }

    private void displayMarker(int index) {
        if(tDatabase.length() == 0) {
            // Top card stuff
            ((TextView) findViewById(R.id.text_parking_date)).setText("No Parking Markers Found");
            ((TextView) findViewById(R.id.text_parking_latlng)).setText("Please add your first parking marker to get started.");
            // Map stuff
            ((CardView) findViewById(R.id.card_parking_map)).setVisibility(View.GONE);
        } else {
            // Map stuff
            ((CardView) findViewById(R.id.card_parking_map)).setVisibility(View.VISIBLE);
            if(marker != null) marker.remove();
            CoorData coorData = tDatabase.getData(index);
            LatLng latLng = new LatLng(coorData.lat, coorData.lon);
            marker = mMap.addMarker(new MarkerOptions().position(latLng).title(coorData.desc));
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                    .target(latLng).zoom(16).build()));
            // Top card stuff
            ((TextView) findViewById(R.id.text_parking_date)).setText(coorData.desc);
            ((TextView) findViewById(R.id.text_parking_latlng)).setText(latLng.toString());
        }
        // Bottom card stuff
        ((Button) findViewById(R.id.button_parking_older)).setEnabled(currentMarker > 0);
        ((Button) findViewById(R.id.button_parking_newer)).setEnabled(currentMarker < tDatabase.length() - 1);
        ((FloatingActionButton) findViewById(R.id.button_parking_add)).setEnabled(lastLoc != null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    protected void onPause() {
        super.onPause();
        writeFile("parking.ini", tDatabase, 2);
    }

    @Override
    protected void onDestroy() {
        tDatabase.clear();
        tDatabase.updateFromDatabase();
        super.onDestroy();
    }

    public void onClicker(View view) {
        Context context = getApplicationContext();
        switch(view.getId()) {
            case R.id.button_parking_newer:
                displayMarker(++currentMarker);
                break;
            case R.id.button_parking_older:
                displayMarker(--currentMarker);
                break;
            case R.id.button_parking_add:
                String desc = f.generateValidPositionName(getApplicationContext(),new Date().toString() + " ");
                tDatabase.change(desc, lastLoc.getLatitude(), lastLoc.getLongitude());
                tDatabase.updateToDatabase(2, desc);
                currentMarker = tDatabase.length() - 1;
                displayMarker(currentMarker);
                break;
            default:
                break;
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        String gmapLink = "http://maps.google.com/maps?q=loc:" +
                latLng.latitude + "," + latLng.longitude + " (" +
                ((TextView) findViewById(R.id.text_parking_date)).getText() + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(gmapLink));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplicationContext().startActivity(intent);
    }

    /**
     * Listenes to the location, and gets the most precise recent location.
     */
    /**
     * Listenes to the location, and gets the most precise recent location.
     */
    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            boolean isBetter = ((lastLoc == null) ||
                    location.getAccuracy() < lastLoc.getAccuracy() + (location.getTime() - lastLoc.getTime()));
            if(isBetter) {
                lastLoc = location;
                ((FloatingActionButton) findViewById(R.id.button_parking_add)).setEnabled(true);
            }
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
        @Override
        public void onProviderEnabled(String provider) {
            ((TextView)findViewById(R.id.text_location)).setText(getString(R.string.text_location_loading));
            onLocationEnabled();
        }

        @Override
        public void onProviderDisabled(String provider) {
            ((TextView)findViewById(R.id.text_location)).setText(getString(R.string.text_location_disabled));
            onLocationDisabled();
        }
    };
    // Snackbar code
    public Snackbar snackbar;
    public void onLocationDisabled() {
        snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), "Location services is disabled", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Enable", onSnackbarClickListener).show();
        ((FloatingActionButton) findViewById(R.id.button_parking_add)).setEnabled(false);
    }
    public void onLocationEnabled() {
        if(snackbar != null) snackbar.dismiss();
        ((FloatingActionButton) findViewById(R.id.button_parking_add)).setEnabled(true);
    }
    final View.OnClickListener onSnackbarClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    };

    // writes currentMarker status of parking and manual save databases to a file so info can be saved
    public void writeFile(String filename, CoorList Database, int select) {
        Context context = getApplicationContext();
        PrintWriter out;
        try {
            out = new PrintWriter(new File(context.getFilesDir(), filename));
        } catch (FileNotFoundException e) {
            // what do you do when built in methods don't even work
            return;
        }
        out.println("Warning: Do Not Edit This File!");
        out.print(Database.dataBaseToString(select));
        out.close();
    }
}
