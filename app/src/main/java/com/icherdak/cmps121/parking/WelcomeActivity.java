package com.icherdak.cmps121.parking;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        TextView tv = (TextView) findViewById(R.id.hello);
        CharSequence cs = "Welcome to the All Purpose Parking app\n" +
                "Start by adding some preferred locations.\n" +
                "Quite a few locations already exist in the default database to help you get started.\n" +
                "To find a location, click find location. You can change your filter preferences in settings.\n" +
                "This will take you to a google maps page with the location you clicked pinned.\n" +
                "You can now route to and from the location easily.\n" +
                "\nIf you are parking your car in a hard-to-locate area, use the parking locator:\n" +
                "Just save your location by pressing the green plus button and you can even view previous parking spots with the \"older\" button\n" +
                "\nTry to explore the app for yourself: the best way to learn is to try for yourself.";
        tv.setText(cs);
    }
}
